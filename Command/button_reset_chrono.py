#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sgc


class Button_reset_chrono(sgc.Button):

    def __init__(self, switch_chrono, label_chrono_user):
        sgc.Button.__init__(self, (100, 30), label="Reset")
        self.switch_chrono = switch_chrono
        self.label_chrono_user = label_chrono_user

    def on_click(self):
        self.switch_chrono.config(state=False)
        self.label_chrono_user.set(0)
