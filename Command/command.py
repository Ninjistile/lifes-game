#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Command/command.py


import sgc
from Tools import readfirst
from Command.button_reset_chrono import Button_reset_chrono
from Command.label_int import Label_int
from Command.combo_pattern import Combo_pattern
from Command.scale_speed import Scale_speed
from Command.button_next import Button_next
from Command.button_reset_board import Button_reset_board

class Command(sgc.VBox):

    def __init__(s, game, posx):
        sgc.VBox.__init__(s, (100, 450), pos = (posx, 0))
        s.game = game

        # Variables importantes
        s.chrono_user = [0]
        s.chrono_general = 0
        s.patterns = readfirst.get_values_from_multiple_file("Config/patterns/")

        list_patterns = []

        for pattern in s.patterns:
            list_patterns.append(pattern)


        #W idgets
        s.scale_speed = Scale_speed()
        s.label_chrono_general = Label_int("General : ")
        s.label_chrono_user = Label_int("User :")
        s.switch_chrono = sgc.Switch((100, 30), label="Chrono", label_side="top", state=False, focus=0)
        s.button_reset_chrono = Button_reset_chrono(s.switch_chrono, s.label_chrono_user)
        s.combo_pattern = Combo_pattern(list_patterns)
        s.switch_running = sgc.Switch((100, 30), label="Running", label_side="top", state=False)
        s.button_next = Button_next(s.game)
        s.button_reset_board = Button_reset_board(s.game, s.button_reset_chrono, s.label_chrono_general, s.switch_running)

        # Ajoute les widget au container box
        s.config(widgets=[s.scale_speed,
                          s.label_chrono_general,
                          s.label_chrono_user,
                          s.switch_chrono,
                          s.button_reset_chrono,
                          s.combo_pattern,
                          s.switch_running,
                          s.button_next,
                          s.button_reset_board])
        s.add(0)

    def refresh_chronos(s):
        s.label_chrono_general.plus()
        if s.switch_chrono.state:
            s.label_chrono_user.plus()