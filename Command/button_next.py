#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sgc


class Button_next(sgc.Button):

    def __init__(s, game):
        sgc.Button.__init__(s, (100, 30), label="Next")
        s.game = game

    def on_click(s):
        s.game.refresh(True)
