#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sgc


class Combo_pattern(sgc.Combo):

    def __init__(self, patterns):
        sgc.Combo.__init__(self, (100, 30), label="Pattern selection", label_side="top",
                 values=patterns, selection=0)
        self.patterns = patterns
        self.pattern = "Born.ini"


    def on_select(self):
        self.pattern = self.patterns[self.selection]