#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sgc


class Switch_run(sgc.Switch):

    def __init__(s, size, label, state):
        sgc.Switch.__init__(s, size, label=label, label_side="top")
        s.run = True
        if state != True:
            s.on_click()

    def on_click(s):
        if s.run:
            s.run = False
        else:
            s.run = True
