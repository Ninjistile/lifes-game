#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sgc


class Button_save(sgc.Button):

    def __init__(s, command):
        sgc.Button.__init__(s, (100, 30), label="Save")
        s.command = command

    def on_click(s):
        if s.command.switch_running.run:
            s.command.switch_running.on_click()