#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sgc


class Button_reset_board(sgc.Button):

    def __init__(s, game, button_reset_chrono, label_chrono_general, switch_running):
        sgc.Button.__init__(s, (100, 30), label="Reset board")
        s.game = game
        s.button_reset_chrono = button_reset_chrono
        s.label_chrono_general = label_chrono_general
        s.switch_running = switch_running

    def on_click(s):
        s.game.board.reset_board()
        s.button_reset_chrono.on_click()
        s.label_chrono_general.set(0)
        s.switch_running.config(state=False)