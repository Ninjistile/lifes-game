#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sgc


class Label_int(sgc.Label):

    def __init__(self, label):
        self.number = 0
        self.label = label
        sgc.Label.__init__(self, (100, 30), text=self.label + str(self.number))

    def plus(self):
        self.number += 1
        self.draw()

    def set(self, num):
        self.number = num
        self.draw()

    def draw(self):
        self.config(text=self.label + str(self.number))