#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sgc


class Scale_speed(sgc.Scale):

    def __init__(s):
        sgc.Scale.__init__(s, (100, 30), label="Wait (ms)", label_side="top",
                           min=0, max=100, min_step=1, small_step=2,
                           max_step=5, show_value=0)