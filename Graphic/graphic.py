#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame
import sgc


class Graphic:

    def __init__(s, board_size, length_cell, color0, color1):
        # board size : liste avec largeur et longueur du tableau de cellule ( en cellule)
        # lenght cell : taille des carrés représentant une cellule
        # color 0 : couleur représentant les cellules mortes
        # color 1 : couleur représentant les cellules vivantes

        pygame.init()
        s.length_cell = length_cell
        s.board_size = board_size
        s.graphic_board_size = [s*length_cell for s in board_size]  # taille en pixel du tableau
        s.graphic_rect_board = pygame.Rect((0,0), s.graphic_board_size)   # Rectangle du tableau

        # Determination de la taille de la fenêtre pour avoir les contrôles
        s.size_window = s.graphic_board_size[:]
        s.size_window[0] += 102
        if s.size_window[1] < 450:
            s.size_window[1] = 450
        s.posx_command = s.graphic_board_size[0]+2                         # position en x du pannel de commandes
        s.rect_command = pygame.Rect((s.posx_command,0), (102,450))        # Rectangle comportant les commandes

        s.window = sgc.surface.Screen(s.size_window)  # créer fenêtre avec le module sgc

        s.color0 = color0
        s.color1 = color1

        #Création d'un tableau avec tout les rectangles correspondant à une cellule sur la fenêtre
        s.graphic_cells = []
        for x in range(board_size[0]):
            s.graphic_cells.append([])
            for y in range(board_size[1]):
                s.graphic_cells[x].append(pygame.Rect((x*length_cell, y*length_cell), (length_cell, length_cell)))

        s.refresh_screen()

    def draw_pixel(s, position, living):
        # Effet de bord : Colore la cellule selon l'état de la cellule
        if living:
            color = s.color1
        else:
            color = s.color0
        pygame.draw.rect(s.window.image, color, s.graphic_cells[position[0]][position[1]])

    def get_relative_position(s, position):
        return [p//s.length_cell for p in position] # Permet de savoir sur quelle cellule la souris est positionné

    def refresh_screen(s):
        sgc.update(100000)     # Raffraichi les commandes
        pygame.display.update(s.rect_command)           # Raffraichi le rectangle contenant les commandes
        pygame.display.update(s.graphic_rect_board)     # Raffraichi le rectangle contenant le tableau

        pygame.draw.rect(s.window.image, pygame.color.Color("black"), s.rect_command)   # Rempli de noir les command (nécessité pour le module sgc)