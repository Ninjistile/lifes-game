#!/usr/bin/python3
# -*- coding: utf-8 -*-

from Entitiy.cell import Cell


class Board:

    def __init__(s, size, graphic):
        # size = longueur et largeur du tableau
        # graphic : objet graphique
        s.size = size
        s.graphic = graphic

        s.cells = [] # initisialisation du tableau de cellules
        for x in range(size[0]):
            s.cells.append([])
            for y in range(size[1]):
                s.cells[x].append(Cell([x, y], False, s))
                # ajoute des cellules au tableau

        # Détermination des cellules aux alentours de chaque cellules
        for colonne in s.cells:
            for cell in colonne:
                cell.set_cell_around()
                

    def refresh(s):
        for colonne in s.cells:
            for cell in colonne:
                cell.refresh_first()

        for colonne in s.cells:
            for cell in colonne:
                cell.refresh_last()

    def kill(s, pos):
        s.cells[pos[0]][pos[1]].set_living(False)
        s.cells[pos[0]][pos[1]].draw(False)

    def born(s, pos):
        s.cells[pos[0]][pos[1]].set_living(True)
        s.cells[pos[0]][pos[1]].draw(True)


    def pattern(s, pos, pattern):
        # Test si le pattern rentre dans la fenêtre
        if pos[0] + len(pattern) - 1 < s.size[0] and pos[1] + len(pattern[0]) - 1 < s.size[1]:
            for x in range(len(pattern)):
                for y in range(len(pattern[x])):
                    if pattern[x][y] == 0:
                        s.kill((pos[0]+x,pos[1]+y))
                    else:
                        s.born((pos[0]+x,pos[1]+y))
            s.graphic.refresh_screen()

    def reset_board(s):
        # Réinitialise toutes les cellules
        for colonne in s.cells:
            for cell in colonne:
                cell.set_living(False)
        s.graphic.refresh_screen()