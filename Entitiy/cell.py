#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Cell:

    def __init__(s, position, living, board):
        # position : coordonnées dans le tableau
        # living : état de la cellule
        # board : objet board
        s.position = position
        s.living = living
        s.will_living = living
        s.board = board

        s.cell_around = [] # liste des cellules alentours
        s.draw(living) # dessine la cellule

    def living_around(s):
        living_around = 0
        for cell in s.cell_around:
            if cell.living:
                living_around+=1
        return living_around

    def set_living(s, state):
        if s.living != state:
            s.draw(state)
            s.living = state
        else:
            s.living = state

    def set_cell_around(s):
        for x in range(-1, 2):
            for y in range(-1, 2):
                if not(x == 0 and y == 0):
                    if 0 <= s.position[0] + x < s.board.size[0] and 0 <= s.position[1] + y < s.board.size[1]:
                        s.cell_around.append(s.board.cells[s.position[0] + x][s.position[1] + y])
                        # ajoute les cellules autour


    def refresh_first(s):
        living_around = s.living_around()
        if s.living:
            if 2 <= living_around <= 3:
                s.will_living = True

            else:
                s.will_living = False
                s.draw(False)
        else:
            if living_around == 3:
                s.will_living = True
                s.draw(True)
            else:
                s.will_living = False

    def refresh_last(s):
        s.living = s.will_living

    def draw(s, state):
        s.board.graphic.draw_pixel(s.position, state)
