#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import pygame

''' Cette classe permet de renvoyer un dictionnaire de tout les informations d'un fichier'''


def get_values_from_file(file_path):
    file = open(file_path, 'r')

    rpattern = file.read()
    rpattern = rpattern.split('\n')
    pattern = []
    for i in range(len(rpattern[0])):
        pattern.append([])

    for y in range(len(rpattern)):
        for x in range(len(rpattern[y])):
            pattern[x].append(int(rpattern[y][x]))

    file.close()
    return pattern


def get_values_from_multiple_file(folder_path):
    files = os.listdir(folder_path)
    values = {}
    for name_file in files:
        values[name_file] = get_values_from_file(folder_path + name_file)

    return values