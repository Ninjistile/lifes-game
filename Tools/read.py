#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import pygame

''' Cette classe permet de renvoyer un dictionnaire de tout les informations d'un fichier'''


def get_values_from_file(file_path):
    file = open(file_path, 'r')

    rpattern = file.read()
    rpattern = rpattern.replace('\n', '')
    rpattern = rpattern.replace(' ', '')
    rpattern = rpattern.split(';')
    pattern = [eval(rpattern[0]), eval(rpattern[1])]
    file.close()
    return pattern


def get_values_from_multiple_file(folder_path):
    files = os.listdir(folder_path)
    values = {}
    for name_file in files:
        values[name_file] = get_values_from_file(folder_path + name_file)

    return values