#!/usr/bin/python3
# -*- coding: utf-8 -*-

from Entitiy.cell import Cell
from Entitiy.board import Board
from Graphic.graphic import Graphic
from Input.input import Input
from Tools import readfirst
from Command.command import Command
from threading import Thread
import pygame


class Game:

    def __init__(s, board_size, length_cell, color0, color1):
        # board size : liste avec largeur et longueur du tableau de cellule ( en cellule)
        # lenght cell : taille des carrés représentant une cellule
        # color 0 : couleur représentant les cellules mortes
        # color 1 : couleur représentant les cellules vivantes
        s.board_size = board_size
        s.graphic = Graphic(board_size, length_cell, color0, color1) # créer le module graphique

        s.input = Input(s.graphic) # créer le module de l'entrée des touches

        s.board = Board(board_size, s.graphic)
        s.length_cell = length_cell

        s.command = Command(s, s.graphic.posx_command) #créer le module commande
        s.graphic.refresh_screen()

        s.wait_done = s.command.scale_speed.value # variable pour la vitesse d'exectution
        s.wait = s.wait_done

    def refresh(s, next):
        # Controle le temps d'attente de chaque ittération => La vitesse
        if s.wait_done != s.command.scale_speed.value:
            s.wait_done = s.command.scale_speed.value
            s.wait = s.wait_done
        if s.wait>0 and s.command.switch_running.state:
            s.wait -= 1

        # Raffraichi les entrers claviers/souris
        s.input.refresh()

        # Conséquence des entrées
        if s.input.click:               # Le clique fait naitre le pattern séléctionné
            s.board.pattern(s.input.mouse_position,
                            s.command.patterns[s.command.combo_pattern.pattern])
        if s.input.active_space_bar:    # La barre espace met en pose
            if s.command.switch_running.state:
                s.command.switch_running.config(state=False)
            else:
                s.command.switch_running.config(state=True)

        # Actions sur le tableau et affichage
        if (s.command.switch_running.state and s.wait <= 0) or next:       # Test pour une nouvelle ittération
            s.command.refresh_chronos()                         # Incrémente les chronos
            s.board.refresh()                                   # Raffraichi les le tableau et les cellules
            s.wait = s.wait_done                                # Raffraichi l'attente
            s.graphic.refresh_screen()
        else:
            s.graphic.refresh_screen()