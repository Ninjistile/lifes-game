#!/usr/bin/python3
# -*- coding: utf-8 -*-


from tkinter import *
from game import Game
import pygame

class Launcher:

    def __init__(s):
        s.do = False
        s.window = Tk()
        s.window.title("Life's Game")
        s.window.resizable(width=False, height=False)

        s.label_name = Label(s.window, text = "Life's Game")
        s.label_name.pack()

        # Choix des dimensions
        s.frame_size = LabelFrame(s.window, text="Dimensions")
        s.frame_size.pack()

        s.label_x = Label(s.frame_size, text="Largeur")
        s.label_x.pack(side='left')

        s.str_x = StringVar()
        s.entry_x = Entry(s.frame_size, textvariable=s.str_x, width=13)
        s.entry_x.pack(side='left')

        s.label_y = Label(s.frame_size, text="Hauteur")
        s.label_y.pack(side='left')

        s.str_y = StringVar()
        s.entry_y = Entry(s.frame_size, textvariable=s.str_y, width=13)
        s.entry_y.pack(side='left')

        s.label_error = Label(s.window, fg="red", text="Veuillez entrer des valeurs correct")

        # Choix taille des cellules
        s.int_length_cell = IntVar()
        s.scale_length_cell = Scale(s.window, orient="horizontal", from_=0, to=30, resolution=1, tickinterval=5,
                                    length=300, label="Taille des cellules :", variable=s.int_length_cell)
        s.scale_length_cell.pack()

        # choix des couleurs
        s.frame_colors = LabelFrame(s.window)
        s.frame_colors.pack()

        s.label_color0 = Label(s.frame_colors, text="Cellules vivantes : ")
        s.label_color0.pack(side='left')

        s.rgbcolor0 = [0, 0, 255]		# Liste RGB de la couleur des cellules Mortes
        s.button_color0 = Button(s.frame_colors, command=s.selec_color0, bg=s.rgb_to_hex(s.rgbcolor0), width=10)	# Ouvre une fenetre de selection de couleur
        s.button_color0.pack(side='left')

        s.label_color1 = Label(s.frame_colors, text="Cellules mortes : ")
        s.label_color1.pack(side='left')

        s.rgbcolor1 = [255, 0, 0]		# Liste RGB de la couleur des cellules Vivantes
        s.button_color1 = Button(s.frame_colors, command=s.selec_color1, bg=s.rgb_to_hex(s.rgbcolor1), width=10)	# Ouvre une fenetre de selection de couleur
        s.button_color1.pack(side='left')

        s.launch= Button (s.window, text="LAUNCH", command=s.launch)
        s.launch.pack()

        s.window.mainloop()

    def rgb_to_hex(s, rgb):
        return '#%02x%02x%02x' % (rgb[0], rgb[1], rgb[2])

    def selec_color0(s):
        s.selec_color(s.rgbcolor0, "Dead cell's color", "Couleur des cellules mortes", False)

    def selec_color1(s):
        s.selec_color(s.rgbcolor1, "Living cell's color", "Couleur des cellules vivantes", True)

    def selec_color(s, rgb, title, label, state):
        s.state = state			# True : Couleur cellules vivantes. False : Couleur cellules mortes.
        s.top_color = Toplevel(s.window) # Nouvelle fenetre
        s.top_color.title(title)
        s.top_color.resizable(width=False, height=False)
        s.color = rgb 			# Couleur creer
        s.frame_selec = LabelFrame(s.top_color, text=label)
        s.frame_selec.pack(side="left")

        s.int_r = IntVar()
        s.int_r.set(s.color[0])
        s.scale_r = Scale(s.frame_selec, orient="horizontal", from_=0, to=255, resolution=1, tickinterval=30,
                                    length=350, label="Rouge", variable=s.int_r)
        s.scale_r.pack()
        s.int_g = IntVar()
        s.int_g.set(s.color[1])
        s.scale_g = Scale(s.frame_selec, orient="horizontal", from_=0, to=255, resolution=1, tickinterval=30,
                                    length=350, label="Vert", variable=s.int_g)
        s.scale_g.pack()
        s.int_b = IntVar()
        s.int_b.set(s.color[2])
        s.scale_b = Scale(s.frame_selec, orient="horizontal", from_=0, to=255, resolution=1, tickinterval=30,
                                    length=350, label="Bleu", variable=s.int_b)
        s.scale_b.pack()
        s.color = s.rgb_to_hex([s.int_r.get(),s.int_g.get(),s.int_b.get()])

        s.button_selec = Button(s.top_color, bg=s.color, command=s.out_color, width=15, height=16)
        s.button_selec.pack(side="right")

        s.button_apply = Button(s.frame_selec, text='Apply', command=s.save_color)
        s.button_apply.pack()

    def save_color(s):
        if s.state:
            s.rgbcolor1=[s.int_r.get(), s.int_g.get(), s.int_b.get()]
            s.button_color1.config(bg=s.color)
        else:
            s.rgbcolor0=[s.int_r.get(), s.int_g.get(), s.int_b.get()]
            s.button_color0.config(bg=s.color)
        s.top_color.destroy()	# Detruit la fenetre

    def out_color(s):
        s.color = s.rgb_to_hex([s.int_r.get(), s.int_g.get(), s.int_b.get()])
        s.button_selec.config(bg=s.color)	# Affiche la couleur cree

    def show_error(s):
        s.label_error.pack()

    def test_error(s):
        # Si ce ne sont que des chiffres, renvoie False. Si une erreur, renvoie False
        for l in s.entry_x.get():
            if ord(l) < 48 or 57 < ord(l):
                return True
        for l in s.entry_y.get():
            if ord(l) < 48 or 57 < ord(l):
                return True
        return False

    def launch(s):
        if s.test_error():
            s.show_error()
        else:
            s.window.quit()
            s.do = True
            

launcher = Launcher()

do = launcher.do
if do:
    print(launcher.rgbcolor0)
    print(launcher.rgbcolor1)
    game = Game((int(launcher.entry_x.get()), int(launcher.entry_y.get())), launcher.scale_length_cell.get(),
                pygame.Color(launcher.rgbcolor1[0], launcher.rgbcolor1[1], launcher.rgbcolor1[2], 255),
                pygame.Color(launcher.rgbcolor0[0], launcher.rgbcolor0[1], launcher.rgbcolor0[2], 255))
    launcher.window.destroy()
    launcher = None		# Libere de la memoire par le garbage collector

    while do:
        game.refresh(False)
