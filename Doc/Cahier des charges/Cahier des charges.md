# Game's Life
---

## Principes fondamentaux

Le jeu se compose d'un tableau en 2 dimensions de taille donné. Chaque case est composé d'une *cellule*. Celles-ci peuvent avoir 2 états, accompagné d'une règle chacun :
* Vivante
    * S'il y a 2 ou 3 cellules adjacentes vivantes, elle survit
* Morte
    * S'il y a 3 cellules adjacentes vivantes, elle vie

---

## Organisation du programme

### Démarage

La fenêtre de démarrage permet de paramétrer le jeu et ensuite le lancer par un boutton.
Les paramètre :
* Taille du tableau (Entrer texte)
    * Largeur
    * Hauteur
* Taille graphique des cellules (Entrer texte)
* Couleurs (Fenêtre de selection)
    * Des cellules vivantes
    * Des cellules mortes

#### Fenètre de selection de couleurs

Une fenêtre de sélection de couleur doit être composé de 3 échelles pour pouvoir faire varier les paramètres RGB.
Un boutton permet de quitter et enregistrer les paramètres établies.

### Fenêtre du jeu

La fenêtre est composé de 2 éléments :
* Le tableau avec les cellules à gauche
* Le panneau d'options à droite

#### Le tableau

Il est composé du nombre de cellules de taille et de couleur selon leur état comme sélectionné depuis la fenêtre de départ.

#### Les options

Les options comprennent :
* La vitesse déxécution du programe
* 2 "chronomètres"
    * Un depuis le lancement
    * Un paramétré par l'utilisateur
* Sélection d'un pattern
* Mise en pause

Les entrer clavier/souris se constituent ainsi :
* Barre espace : Mise en pause/Mise en marche
* Clique gauche : Sur le tableau, fait apparaitre le pattern choisi
