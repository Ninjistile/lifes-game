# Life's Game, ajouts

--------

## Fenêtre d'accueil


* Titre
* Hauteur / Largeur
* Couleur 0 / Couleur 1
* Développeurs

## Tableau

Done

## Fenêtre de commande

1. Vitesse du jeu
2. Génération de paterne
    * Faire vivre
    * Faire Mourir
    * Produire un paterne connu à partir d'un fichier
3. Compteur d'itération
  * Depuis le début
  * Compteur d'itération initialisé par l'utilisateur
4. Fichier Meta
  * Sauvegarder état
  * Sauvegarder chaque itération
  * Charger fichier
5. Indicateur de Pause
