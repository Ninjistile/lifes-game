#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame
import sgc


class Input:

    def __init__(self, graphic):
        # graphic : l'objet graphique

        self.id_space_bar = 32
        self.active_space_bar = False
        self.id_click = 1
        self.click = False
        self.mouse_position = None
        self.graphic = graphic

    def refresh(self):
        self.active_space_bar = False
        for event in pygame.event.get():
            sgc.event(event)
            if event.type == pygame.QUIT:
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == self.id_space_bar:
                    self.active_space_bar = True
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == self.id_click:
                    self.click = True
            if event.type == pygame.MOUSEBUTTONUP:
                if event.button == self.id_click:
                    self.click = False

        self.mouse_position = self.graphic.get_relative_position(pygame.mouse.get_pos())